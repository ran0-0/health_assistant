**数据表使用 ha_ 前缀**

## 团体表

团体列表：班级、公司、部门

```
ha_group_list
{
    _id: "", // string，自生成
    parent_id: "", // 所属ID
    name: "", // 团体名称，如"四（3）班"、"软件2组"、"财务部"、"xx公司"等
    account_id: "", // string 管理员账号ID（用户账号）
    is_reg: 0, // int 是否开放成员注册 1开放
    <!-- people_total: 0, // 成员总数，TODO，待删除 -->
    order: 1, // int，排序
}
```

团体成员名单：团体管理员导入的名单

```
ha_group_person
{
    _id: "", // string，自生成
    group_id: "", // string 团体ID
    person_name: "", // string 成员名字
    person_identity: "", // string 成员身份证号
    person_sex: 0, // int, 性别1男2女3保密
    person_num: 0, // int 成员编号（学号、工号等）
    person_remark: "", // string 成员备注（如部门中的某分组不再建立团体，就写入备注）
    order: 1, // int，排序
}
```

团体通知：暂时不用

```
ha_group_notice
{
    _id: "", // string，自生成
    group_id: "", // string 团体ID
    order: 1, // int，排序
}
```

## 用户表

用户账号表：

```
ha_user_account
{
    _id: "", // string，自生成
    username: "", // string 用户
    password: "", // string 密码(禁止明文)
    mobile: "", // string 手机号，需验证符合规则
    email: "", // string 邮箱
    wx_open_id: "", // string 关联微信openid  用户code 换取 暂不用，后续和支付宝等一起考虑 TODO
    <!-- class_info: "", // string 管理班级ID  一期仅支持一个老师管理一个班级 待删除 TODO -->
}
```

个人信息表：暂时不用

```
ha_user_person
{
    _id: "", // string，自生成
    name: "", // string 用户名字
    identity: "", // string 身份证号
    sex: 0, // int, 性别1男2女3保密
    symbol: "" // string 关系、备注等
}
```

用户的报备模板：包含导入自建、收藏、上级团队下发的等，一期第二阶段使用

```
ha_user_template
{
    _id: "", // string，自生成
    account_id: "", // string 用户账号“ha_user_account” ID
    template_id: "", // string 模板“ha_report_template” ID
    get_way: 0, // int 来源 0未知，1导入自建或收藏，2上级下发
}
```

## 关系表

用户账号和个人信息：

```
ha_relation_user
{
    _id: "", // string，自生成
    account_id: "", // string 用户账号ID
    person_id: "", // string 个人信息ID
}
```

## 资源表

报备模板：一期第二阶段使用

```
ha_report_template
{
    _id: "", // string，自生成
    name: "", // string 模板名称
    <!-- label: "", // string 模板标签 TODO 可以不用 -->
    is_open: 0, // int 是否公开1公开，其他不公开（前端不能改，只有后台才可以设为公开，公开的所有用户都可以看到）
    content: "", // string 模板内容，一段html代码或其他方式，含所有字段
}
```

报备列表：

```
ha_report_list
{
    _id: "", // string，自生成
    group_id: "", // string 所属团体ID
    is_active: 1, // int 是否激活
    interval_hour: 0, // int 间隔小时数0表示只能提交单次，其他间隔时间可以重复提交
    content: "", // string 报备内容，一段html代码或其他方式，含所有字段
}
```

报备记录：

```
ha_report_record
{
    _id: "", // string，自生成
    report_id: "", // string 所属reportID
    person_gid: "", // string 团体成员ID
    create_time: 0, // int 时间戳 GMT
    allow_modify: 0, // int 是否允许修改1允许
    // 以下由报备内容生成的所有字段，考虑使用统一的前缀
}
```

报备汇总：暂时不用

```
ha_report_summary
{
    _id: "", // string，自生成
    group_id: "", // string 所属团体ID
}
```

<!-- ## 每日健康报备

```
ha_daily_report_log TODO 待更改
{
    _id: "", // string，自生成
    create_time: 0, // int 时间戳 GMT
    class_id:"",//string 班级标志
	stu_id: "", // string 学生唯一标识，可选
    stu_num:"",//学号
    stu_name: "", // string 学生姓名
	contact_virus:0, // 有无接触湖北/武汉人员 int 0无，1有
	have_symptom:0, // 有无疑似症状 int 0无，1有
	current_pos:"", //当前位置，0：本地，1：外地（除湖北），2：外地（湖北）
    health:""//当前健康状况，多选，可选项：good/kesou/fali/fare，多选时逗号分隔,良好不能好其他多选
	temperature: 0, // 体温，可选
}
``` -->


<!-- 年级表：TODO 要删除
```
ha_grade_list
{
    _id: "", // string，自生成
    name:'', //一年级、二年级等
    order:1'' //年级排序
}
``` -->

<!-- 学生表：

```
ha_students TODO 要删除
{
    _id: "", // string，自生成
    username: "", // string 用户
    password: "", // string 密码(禁止明文)
    wx_open_id: "", // string 关联微信openid  用户code 换取
    stu_num: "", // string 学号 student number
    stu_name: "", // string 学生姓名
    class_id: "", // string 学生所属班级ID
}
``` -->

<!-- 学生家长表：

```
ha_parents TODO 要删除
{
    _id: "", // string，自生成
    username: "", // string 用户
    password: "", // string 密码(禁止明文)
    wx_open_id: "", // string 关联微信openid  用户code 换取
    class_id: "", // string 学生所属班级ID
    stu_id:"",//学校若提前录入学生信息，则可绑定；若没有学生信息表，本项可空
    stu_num: "", // string 学号 student number
    stu_name: "", // string 学生姓名
}
``` -->

<!-- ha_admin_users: 目前不清楚什么用途

Tips：支持一个学生关联多个家长，可能每次报备的家长不同。 -->

<!-- 暂不支持学校表
```
school
{
    _id: "", // string，自生成
    name: "", // string 学校名称不能为空
    phone: "", // string 联系方式不能为空，需验证符合规则
    address: "" // string 学校地址
	create_time: 0, // 时间戳 GMT
}
```
 -->

<!--
#### 成员表
```
member
{
    _id: "", // string，自生成
    guid: "", // string 用户唯一标识，添加时生成无法修改
    id_type: 0, // int 证件类型 0 身份证, 1 护照
    id_card: "", // string 证件号码，需验证符合规则
    name: "", // string 不能为空
    phone: "", // string 不能为空，需验证符合规则
    age: 18, // int
    sex: 0, // int (0女, 1男, 2未知)
    photo: "", // string 图片url地址
	school_id:"" //成员属于学校(新增了一个学院表id)
}
```

 #### 成员操作历史
```
// 增加人员时需要写入
member_opera_history
{
    _id: "", // string，自生成
    user_guid: "", // string 用户唯一标识
    member_id: 0, // string 成员唯一标识
    status: 0, // int 0新增，1删除
    create_time: 0, // int 时间戳 GMT
    create_ip: "", // string 当前操作ip
}
```


#### 学生表
```
student
{
    _id: "", // string，自生成
	create_time: 0,// 时间戳 GMT
	class_id:"",//班级唯一标识
    name: "", // string 学生姓名 不能为空
	sex: 0, // int (0女, 1男, 2未知)
    phone: "", // string 家长联系电话，需验证符合规则
	area:0, //归属地  0本地留守,1异地出访（除湖北）,2异地出访（湖北）
	area_info:
	{address:'',phone:"",status:0,comment:""}//本地留守,现住址(address),联系方式(phone),目前健康状况（单选项，候选项如下）0良好,1发热,2乏力,3咳嗽,备注(comment)
	{address:'',leave_info:{leave_time:0,vehicle:'',flight:''},status:0,comment:""}//异地出访,leave_time(离开时间),vehicle(交通工具),flight(航班)
}
```
 -->