// 老师绑定班级信息 深圳-前端-张昊
'use strict';
const db = uniCloud.database()
const dbCmd = db.command
exports.main = async (event, context) => {
	const gradeList = db.collection('ha_grade_list') //  获取年级表
	const groupList = db.collection('ha_group_list') //  获取班级表
	const userAccount = db.collection('ha_user_account') //  获取用户账号表
	let data = {}
	//event为客户端上传的参数
	if (event.parent_id == null) {
		let gradeData = await gradeList.get()
		data = gradeData.data
	} else if (event.class_info == null) {
		let classData = await groupList.where({
			// 获取所属团体的总数
			parent_id: dbCmd.eq(event.parent_id)
		}).get()
		data = classData.data
	} else {
        
        let teacherObj = await userAccount.where({
			tokenSecret: event.token
		}).get()
        
        
        let uid = event.uid
        if(uid){
            userAccount.doc(uid).update({
                class_info:event.class_info
            })
        }
        
        console.log("teacherObj",teacherObj)
        
        // if(teacherObj.data){
        //     teacherObj.update({
        //         class_info:event.class_info
        //     })
        // }
        
        
		// let classData = await userAccount.where({
		// 	// 获取班级表的总数
		// 	tokenSecret: dbCmd.eq(event.token)
		// }).update({
		// 	class_info: event.class_info
		// })
		data = {
			// classData: classData
		}
	}

	//返回数据给客户端
	return data
};
